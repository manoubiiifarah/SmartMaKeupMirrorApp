import express from 'express';
import mongoose from 'mongoose'; // Importer Mongoose

import productRoutes from './routes/product.js';
import userRoutes from './routes/user.js';
import imageRoutes from './routes/image.js';
import categoryRoutes from './routes/category.js';
import favoriteRoutes from './routes/favorite.js';
import productReferenceRoutes from './routes/productReference.js';
import subCategoryRoutes from './routes/subCategory.js';




const app = express();
const port = process.env.PORT || 9090;
const databaseName = 'smartMakeUpMirrorAppDB';


// Cela afichera les requêtes MongoDB dans le terminal
mongoose.set('debug', true);
// Utilisation des promesses ES6 pour Mongoose, donc aucune callback n'est nécessaire
mongoose.Promise = global.Promise;

// Se connecter à MongoDB
mongoose
    .connect(`mongodb://localhost:27017/${databaseName}`)
    .then(() => {
        // Une fois connecté, afficher un message de réussite sur la console
        console.log(`Connected to ${databaseName}`);
    })
    .catch(err => {
        // Si quelque chose ne va pas, afficher l'erreur sur la console
        console.log(err);
    });

app.use(express.json());

app.use('/product', productRoutes);
app.use('/user', userRoutes);
app.use('/image', imageRoutes);
app.use('/category', categoryRoutes);
app.use('/favorite', favoriteRoutes);
app.use('/productReference', productReferenceRoutes);
app.use('/subCategory', subCategoryRoutes);



app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});